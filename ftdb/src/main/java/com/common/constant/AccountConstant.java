/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.common.constant;

import java.math.BigDecimal;

/**
 * 账户配置常量
 *
 */
public class AccountConstant {
	/**
	 * 账户正常
	 */
	public static final String ACCOUNT_NORMAL = "0";
	/**
	 * 已销户
	 */
	public static final String ACCOUNT_SALES = "1";
	/**
	 * 只进不出
	 */
	public static final String ACCOUNT_ONLYIN = "2";
	/**
	 * 只出不进
	 */
	public static final String ACCOUNT_ONLYOUT = "3";
	/**
	 * 不进不出
	 */
	public static final String ACCOUNT_NOINOUT = "4";
	/**
	 * 交易方向-借方
	 */
	public static final String TRANLIST_D = "D";
	/**
	 * 交易方向-贷方
	 */
	public static final String TRANLIST_C = "C";
	/**
	 * 交易成功
	 */
	public static final String ACCOUNT_TRAN_SUCCESS = "1";
	
	/**
	 * 交易失败
	 */
	public static final String ACCOUNT_TRAN_FAILURE = "0";

	
	public static final double ACCOUNT_DEFAULT_MONERY = 10000.00;

	
	public static final BigDecimal ACCOUNT_BIGDECIMAL_MONERY = BigDecimal.valueOf(88888888.88);
	
	public static final int ACCOUNT_SEQ_FAILURE = -1;
	/**
	 * 默认原子性，一致性，测试账户数
	 */
	public static final int ACCOUNT_DEFAULT_NUM = 10000;

}
