/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mapper.DataCleanMapper;
import com.service.DataRecoveryService;

@Service
@Scope("prototype")
public class DataRecoveryServiceImpl implements DataRecoveryService,Runnable{

    private final Logger logger = LoggerFactory.getLogger(DataRecoveryServiceImpl.class); 

    @Autowired
    DataCleanMapper dataCleanMapper;
	public void run() {
		try {
			logger.info(" DataRecoveryServiceImpl begin run ");
			dataCleanMapper.updateBranch();
			dataCleanMapper.updateSjno();
			dataCleanMapper.updateCustomer();
			dataCleanMapper.updateAccount();
			dataCleanMapper.updateAccountERROR();
			dataCleanMapper.updateSjnoBale();
			dataCleanMapper.updateCustomerBale();
						logger.info(" DataRecoveryServiceImpl end run ");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(" DataRecoveryServiceImpl run error {} ",e);
		}
	}

    
}